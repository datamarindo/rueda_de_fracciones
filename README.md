# Ruedas de fracciones
Este script hace ruedas de fracciones, que son herramientas didácticas para aprender fracciones y para ver los tamaños de brocas y tornillos.

Las imágenes son archivos __svg__, por lo cual pueden imprimirse a cualquier tamaño sin perder resolución.

Solo hay que ponerle una cuerda en el centro y desplazarse contando como el reloj para sumar, o al revés del reloj para restar.

## Rueda de fracciones de medios, 4tos, 8vos, 16avos,32avos
<img src="rueda_medios.svg"  width="300" height="300">


## Rueda de fracciones de tercios y medios
<img src="rueda_tercios_medios.svg"  width="300" height="300">

El script se puede personalizar para cambiar los colores o las fracciones (por si se requieren 5tos, 7mos, etc.)

## Rueda tipo blueprint
<img src="rueda_medios_azul.svg"  width="240" height="240">

Referencias:
C. Agostinelli and U. Lund (2017). R package 'circular': Circular
  Statistics (version 0.4-93). 

